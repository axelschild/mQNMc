'''
mQNCc
by Axel Schild

This file is part of mQNCc.

mQNCc is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

mQNCc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with mQNCc.  If not, see <http://www.gnu.org/licenses/>.

This file contains the main class <molecule>.
'''

import numpy as np
import itertools as itol
from copy import copy
from math import factorial as fac
from scipy.interpolate import interpolate as interpol
from mQNMc.tools       import *
from mQNMc.conversion_factors import *

class molecule:
  '''
  Class containing all information that is necessary for the computations of 
  the nuclear density in the local harmonic approximation.
  
  Example program run for the ground state:
    mol = molecule()
    mol.masses = masses
    mol.nuclei = nuclei
    mol.force_constants = force_constants
    mol.X0 = X0
    mol.nquant = [0]*len(force_constants)
    mol.ngrid = 101
    mol.xmax = 1.3
    mol.F_linear = False
    mol.mqnmc()
  where you can try to get nuclei, masses, X0, force_constants from a PSI4
  output with the function read_psi4 from <tools.py>. The PSI4 input needs 
  a current PSI4 version and the lines
    E, wfn = frequency('mp3', return_wfn=True)
    wfn.hessian().print_out()
  to print the Hessian which is <force_constants>.
  
  ****** ALL input quantities need to be in atomic units (see  *****************
  ****** conversion_factors.py for some conversion factors :-) *****************
  
  There should be nnuc nuclei, each having ndim=3, coordinates. The density is 
  computed in displacement coordinates x1, x2, x3 relative to the equilibrium 
  position X0. This grid may either be defined (one for all nuclei) or may be 
  determined automatically depending on the nuclear mass (in this case, all 
  grids are stored in the lists x1rel, x2rel, x3rel (in atomic units)).
  
  Default internal units are strange (length in Angstroem/25, mass in atomic 
  units, time in femtoseconds). You can set mol.F_SI = True to change to 
  SI-based internal units (length in 10^-10 m, mass in 10^-27 kg, time in 
  10^-13 s -> energy in 10^-21 J, hbar = 1.05457180013). If you run mol.mqnmc() 
  you will not notice the internal units, but if you run mol.initialize() you 
  see that the units of (some) input quantities have changed. Call 
  mol.finalize() to convert them back to the original units.
  
  If you use any other units for the input, you need to set the conversion
  factors to internal coordinates accordingly before calling the initialize
  function (as it sets the value of hbar). The internal units are defined by 
  the conversion factors, hence if you set those factors to 1 the calculation 
  will run in the units of the input, whether this is a good idea or not. 
  If you do not use the default internal units some of the printed information
  may be wrong.
  
  Note also: There is a useful convenience function for plotting the normal 
  modes <plot_normal_modes>.
  
  For mol = mqnmc.molecule()
  
  you will get
    mol.rho ............... list of densities for each nucleus for grid in 
                            displacement coordinates (if you want the density 
                            for all nuclei on one grid, look at the function
                            mol.construct_full_density)
   and either, if automatic grid generation is used,
    mol.x1red ............. list of displacement coordinate x1 for each nucleus
    mol.x2red ............. list of displacement coordinate x2 for each nucleus
    mol.x3red ............. list of displacement coordinate x3 for each nucleus
   or you have set mol.x1, mol.x2, mol.x3 which will be the grid in 
   displacement coordinates for all nuclei
  you need to set
    mol.nuclei ............ names of the nuclei; can be strings or numbers or
                            whatever you want; its length is important, 
                            otherwise used only for plotting
    mol.masses ............ numpy array containing the masses for all 
                            nnuc*ndim coordinates in atomic units (may also be 
                            one mass per nucleus, this would be corrected)
    mol.force_constants ... 2D matrix of second derivatives in atomic units
    mol.X0 ................ equilibrium position of nuclei in atomic units, 
                            shape(X0) = (nnuc,ndim)
    mol.nquant ............ quantum numbers of all nnuc*ndim normal modes; the 
                            first 5 or 6 are assumed to be for translation and 
                            rotation
    mol.F_linear .......... set True if molecule is linear, otherwise False
  you should set
    mol.freq_trans_rot .... frequency of Gaussian for translations & rotations 
                            in atomic units; may be one number or numpy array of 
                            ntrans+nrot numbers
   and either
    mol.ngrid .......... . triggers automatic guess for x1, x2, x3 depending on 
                           the mass of each nucleus to compute and gives ngrid 
                           points for each direction; default is 101 (odd 
                           numbers are better...); you need to set this to 0 in
                           order to give your own grid (one for all nuclei!)
    mol.xmax ............. extend of autogrid [default: 1.0]; the automatic 
                           grid will be x < |xmax/sqrt(mass)| with the mass in 
                           atomic mass units (i.e., for a hydrogen nucleus the 
                           grid in x1, x2, x3 direction extends from ca. -xmax
                           to +xmax, for a carbon nucleus it extends exactly 
                           from -xmax/sqrt(12) to +xmax/sqrt(12))
   or
    mol.x1 ............... x-grid for density (displacement coordinates)
    mol.x2 ............... y-grid for density (displacement coordinates)
    mol.x3 ............... z-grid for density (displacement coordinates)
                           will be ignored if mol.ngrid =/= 0
  you may like to set
    mol.c_length ........... length conversion factor to internal units
    mol.c_mass ............. mass conversion factor to internal units
    mol.c_time ............. time conversion factor to internal units
    mol.F_SI ............... change to SI-based internal units (you can try this
                             if your results look strange, but probably this 
                             will not change much...
  you probably don't want to change
    mol.hbar ............... hbar in internal units, computed in mol.initialize()
    mol.lightspeed ......... speed of light in internal units
    mol.c_energy ........... energy conversion factor, computed in mol.initialize()
    mol.ndim ............... number of coordinates per nucleus (currently has to 
                             be 3, otherwise initialize will not work correctly 
                             and wrong results will be ontained)
    mol.nmodes ............. number of normal modes / coordinates
    mol.nrot ............... number of rotational degrees of freedom
    mol.nvib ............... number of vibrational degrees of freedom
    mol.ntrans ............. number of translational degrees of freedom
    mol.frequencies ........ frequencies
    mol.normal_modes ....... normal modes; matrix Q
    mol.normal_modes_nmw ... "non-mass-weighed normal modes" Q_kj/sqrt(M_K)
                             is usually the result from electronic structure 
                             calculations and transfroms the normal mode 
                             coordinates to displacement coordinates:
                              sum_j Q_kj/sqrt(M_K) tilde{q}_j = x_k (cf. notes)
    mol.eigenvalues ........ eigenvalues of force_constants
    mol.tmatrix ............ transformation matrix from Cartesian nuclear
                             coordinates to frequency-scaled normal modes
    mol.jacob .............. Jacobian for that transformation
    mol.a .................. coefficients for the polynomial prefactor
    mol.s .................. coefficients for the polynomial in the Gaussian
    mol.dv ................. volume element
    mol.nx ................. number of points in (x1,x2,x3)-direction
    mol.xmesh .............. mesh of x1-x2-x3-grid
    mol.x1rel .............. list of displacement grid along x1 for each nucleus
    mol.x2rel .............. list of displacement grid along x2 for each nucleus
    mol.x3rel .............. list of displacement grid along x3 for each nucleus
                             all of those will be empty if mol.ngrid = 0, i.e. 
                             if a manual grid for all nuclei is set
  '''
  def __init__(self):
    self.nuclei = None
    self.force_constants = None
    self.masses = None
    self.X0 = None
    self.nmodes = None
    self.ndim = 3
    self.nrot = 3
    self.nvib = None
    self.ntrans = None
    self.freq_trans_rot = 0.5
    self.frequencies = None
    self.normal_modes = None
    self.normal_modes_nmw = None
    self.eigenvalues = None
    self.tmatrix = None
    self.jacob = 1.
    self.a = None
    self.s = None
    self.nquant = []
    self.rho = 0
    self.x1rel = None
    self.x2rel = None
    self.x3rel = None
    self.x1 = None
    self.x2 = None
    self.x3 = None
    self.dv = None
    self.nx = None
    self.xmesh = None
    self.F_linear = False
    self.F_SI = False
    self.ngrid = 201
    self.xmax = 1.0
    self.hbar = None
    self.lightspeed = None
    self.c_energy = None
    self.c_length = copy(length_au__AA) * 25
    self.c_mass = copy(mass_au__u)
    self.c_time = copy(time_au__fs)
  
  def initialize(self):
    '''
    Initialize parameters that have not been set.
    
    The following parameters need to be set before calling self.initialize:
      self.nuclei, self.masses, self.force_constants, self.X0, self.nquant
    '''
    if self.ndim != 3: 
      print('The mQNMc works currently only for ndim = 3!')
      print('The computation will continue but the results should not be trusted.')
    # use SI-based internal units
    if self.F_SI:
      print('Using SI-based interal units...')
      print('NB: If you want to use custom units, set F_SI=False.')
      self.c_length = length_au__AA
      self.c_mass = mass_au__SI * 1e27
      self.c_time = time_au__SI * 1e13
    # set conversion factor for energy
    self.c_energy = self.c_mass * self.c_length**2 / self.c_time**2
    # set hbar 
    self.hbar = self.c_energy * self.c_time
    # set speed of light
    if self.F_SI:
      self.lightspeed = c_SI * self.c_length / self.c_time
    else:
      self.lightspeed = c_au * self.c_length / self.c_time
    # convert input parameters
    self.force_constants *= self.c_mass/self.c_time**2
    self.masses *= self.c_mass
    self.freq_trans_rot /= self.c_time
    # convert grid parameters if given
    if not self.ngrid:
      self.F_autogrid = False
      self.x1 *= self.c_length
      self.x2 *= self.c_length
      self.x3 *= self.c_length
      # set grid parameters
      self.dv = (self.x1[-1]-self.x1[0])/float(len(self.x1)-1) * \
                (self.x2[-1]-self.x2[0])/float(len(self.x2)-1) * \
                (self.x3[-1]-self.x3[0])/float(len(self.x3)-1)
      self.xmesh = np.meshgrid(self.x1, self.x2, self.x3, indexing='ij')
      self.nx = np.array([len(self.x1), len(self.x2), len(self.x3)])
    # determine number of coordinates / normal modes
    self.nmodes = len(self.force_constants)
    # initialize list of densities if computed for each nucleus separately
    self.rho = [[] for ii in range(len(self.nuclei))]
    # set number of rotational degrees of freedom
    if self.F_linear: self.nrot = 2
    # set number of translational degrees of freedom
    self.ntrans = copy(self.ndim)
    # determine number of vibrational degrees of freedom
    self.nvib = self.nmodes - self.ntrans - self.nrot
    # prepare empty lists for automatic grids for each nucleus
    self.x1rel = [[] for ii in range(len(self.nuclei))]
    self.x2rel = [[] for ii in range(len(self.nuclei))]
    self.x3rel = [[] for ii in range(len(self.nuclei))]
    # check if all masses are given or only one per nucleus
    if len(self.masses) != self.nmodes:
      if len(self.masses)*self.ndim == self.nmodes:
        print('Correcting masses such that it contains one mass per coordinate.')
        masses = np.zeros(len(self.nmodes))
        for ii in range(len(self.masses)):
          masses[self.ndim*ii:self.ndim*(ii+1)] = self.masses[ii]
        self.masses = masses
      else:
        print('Array for masses has wrong length.')
    if self.F_SI:
      print('Computation in SI-based units is not implemented.')
  
  def finalize(self):
    # switch all transformed and computed quantities back to input units or 
    # set them to None, if they are suspected not to be needed anymore
    print('Reset all quantities to input values...')
    self.force_constants /= self.c_mass/self.c_time**2
    self.masses /= self.c_mass
    self.freq_trans_rot *= self.c_time
    self.xmesh = None
    self.jacob = None
    self.frequencies *= self.c_time
    try:
      self.x1 /= self.c_length
      self.x2 /= self.c_length
      self.x3 /= self.c_length
      self.dv /= self.c_length**self.ndim
      for ii in range(len(self.rho)):
        self.rho[ii] *= self.c_length**self.ndim
    except:
      print('No density was computed...')
  
  def get_normal_modes(self,trunc=1e-12):
    '''
    Determine normal modes and frequencies by determining eigenvectors and
    eigenvalues of the force constant matrix.
    '''
    # set up mass-weighted force constant matrix
    fc_mw = self.force_constants / np.sqrt(np.outer(self.masses,self.masses))
    # compute eigenvalues and normal modes
    self.eigenvalues, self.normal_modes = np.linalg.eigh(fc_mw)
    # remove values that are too small
    self.eigenvalues[np.abs(self.eigenvalues)<trunc] = 0
    self.normal_modes[np.abs(self.normal_modes)<trunc] = 0
    # non-mass-weighted normal modes (to compare against the output of 
    # electronic structure programs)
    self.normal_modes_nmw = self.normal_modes/np.sqrt(np.reshape(self.masses,(self.nmodes,1)))
    # compute frequencies
    self.frequencies = np.sqrt(np.abs(self.eigenvalues))
  
  def set_width_trans_rot(self):
    ''' 
    Set the frequencies of the normal modes for translation and rotation of the
    whole system such that the harmonic oscillator ground state density at this
    frequencies corresponds to a normal distribution of certain variance:
    
    Note that the variance can be either a single number of an array of numbers
    for each of the considered degrees of freedom.
    '''
    #self.frequencies[0:self.ntrans+self.nrot] = self.hbar/(2*self.variance)
    self.frequencies[0:self.ntrans+self.nrot] = self.freq_trans_rot
    print('Frequencies after setting those for translation & rotation [1/cm]:')
    for freq in self.frequencies:
      if self.F_SI:
        print(freq*self.hbar*energy_SI__icm*1e-21)
      else:
        print(freq*self.hbar/self.c_energy*energy_au__icm)

  def print_information(self):
    '''
    Print information about the system. Only works if input is in atomic units.
    '''
    print('The following is only correct if the input is in atomic units:')
    print('')
    print('Frequencies [1/cm]:')
    for freq in self.frequencies:
      if self.F_SI:
        print(freq*self.hbar*energy_SI__icm*1e-21)
      else:
        print(freq*self.hbar/self.c_energy*energy_au__icm)
    print('')
    print('Quantum numbers in each mode (first 5 or 6 should always be zero):')
    print(self.nquant)
    print('')
  
  def get_transformation_matrix(self):
    '''
    Determine the transformation matrix from frequency-scaled normal mode 
    coordinates to the Cartesian coordinates of the nuclei. Along the way, 
    compute also the Jacobian determinant for the volume element.
    '''
    # compute transformation matrix
    self.tmatrix = np.zeros(np.shape(self.force_constants))
    for jj in range(self.nmodes):
      for kk in range(self.nmodes):
        self.tmatrix[jj,kk] = np.sqrt( self.frequencies[jj] *                  \
                               self.masses[kk]/ self.hbar) *                   \
                                self.normal_modes[kk,jj]
    # NOTE: The Jacobian will be included in self.a and is not needed otherwise
    self.jacob = np.abs(np.linalg.det(self.tmatrix))
  
  def init_poly_param(self):
    '''
    Initialize coefficients of the polynomial prefactor of the Gaussian 
    function.
    '''
    # number of polynomial coefficients per mode
    k_max = np.zeros(self.nmodes,dtype=np.int8)
    # list of polynomial coefficients per mode
    a_ind = [[] for jj in range(self.nmodes)]
    for jj in range(self.nmodes):
      k_max[jj] = 2*int(np.floor(self.nquant[jj]/2))+1
      a_ind[jj] = [[] for kk in range(0,k_max[jj])]
    # compute coefficients per mode
    for jj in range(self.nmodes):
      for kk in range(0,k_max[jj]):
        # boundaries for summation over l to determine c
        l_bound = int(np.floor(self.nquant[jj]/2) -                            \
                   np.abs(kk-np.floor(self.nquant[jj]/2)))
        # get c
        # in the notes, c comes from iterating over list of half integer values
        # here, interger values with step size of 2 are used
        c = 0.
        for ll in range(-l_bound,l_bound+1,2):
          c += 1./(fac((kk+ll)/2) * fac((kk-ll)/2) * \
                fac(self.nquant[jj]-kk-ll) * fac(self.nquant[jj]-kk+ll) )
        c *= (-1)**kk * 2**(self.nquant[jj]-2*kk) * fac(self.nquant[jj]) / np.sqrt(np.pi)
        # get coefficients for monomials for each mode
        a_ind[jj][kk] = np.ones([self.nmodes]*2*(self.nquant[jj]-kk))
        for index in itol.product(range(self.nmodes),repeat=2*(self.nquant[jj]-kk)):
          a_ind[jj][kk][index] *= np.prod(self.tmatrix[jj,index])
        a_ind[jj][kk] *= c
    # compute product of all possible coefficients of different modes
    # strange way to find the indices of the possible combinations for the 
    # product a_ind[0][k0] * a_ind[1][k1] * a_ind[2][k2] * ...
    dummy = list(itol.product(range(int(k_max[0])),range(int(k_max[1]))))
    for ii in range(2,self.nmodes):
      dummy = list(itol.product(dummy,range(int(k_max[ii]))))
    # the strange result has to be flattened
    branch = []
    for ii in range(int(np.prod(k_max))):
      branch.append(list(flatten(dummy[ii])))
    # determine orders of all possible products
    order = []
    for ii in range(int(np.prod(k_max))):
      shape = ()
      for jj in range(self.nmodes):
        shape += np.shape(a_ind[jj][branch[ii][jj]])
      order.append(len(shape))
    # initialize final list of coefficients for all (unique) orders up to twice
    # the sum of the quantum numbers (even if they are zero...)
    self.a = []
    for order_now in range(0,max(np.unique(order))+1,2):
      self.a.append(np.zeros([self.nmodes]*order_now))
    # compute final list of coefficients by computing all possible products and
    # adding those of the same order
    for ii in range(len(order)):
      dummy = a_ind[0][branch[ii][0]]
      for jj in range(1,self.nmodes):
        s1 = np.shape(dummy) + (1,) * len(np.shape(a_ind[jj][branch[ii][jj]]))
        s2 = (1,) * len(np.shape(dummy)) + np.shape(a_ind[jj][branch[ii][jj]])
        dummy = np.reshape(dummy,s1) * np.reshape(a_ind[jj][branch[ii][jj]],s2)
      self.a[order[ii]//2] += dummy
    # multiply initial coefficients with Jacobian determinant
    for ii in range(len(self.a)):
      self.a[ii] *= self.jacob
    
  def init_gaus_param(self):
    '''
    Initialize coefficients of the polynomial in the exponent of the Gaussian 
    function.
    '''
    self.s = np.zeros(np.shape(self.tmatrix))
    for jj in range(self.nmodes):
      for kk in range(self.nmodes):
        for ll in range(self.nmodes):
          self.s[jj,kk] += self.tmatrix[ll,jj] * self.tmatrix[ll,kk]
  
  def integrate(self):
    '''
    Integrate over the last coordinate.
    
    Calls integrate_a and integrate_s to update self.a and self.s, 
    respectively. Order is important, because for integrate_a we need the 
    current self.s.
    '''
    for ii in range(self.nmodes-self.ndim):
      self.integrate_a()
      self.integrate_s()
      #sys.exit()
  
  def integrate_s(self):
    '''
    Update self.s when integrating over the last coordinate.
    '''
    # nothing complicated here
    nn = len(self.s)
    s_new = np.zeros([nn-1,nn-1])
    for jj in range(nn-1):
      for kk in range(nn-1):
        s_new[jj,kk] = self.s[jj,kk] - self.s[jj,nn-1] * self.s[kk,nn-1] \
                        / self.s[nn-1,nn-1]
    self.s = s_new

  def integrate_a(self):
    '''
    Update self.a when integrating over the last coordinate.
    '''
    # something complicated here
    nn = len(self.s)
    a_new = []
    # loop over all orders
    for iam in range(len(self.a)):
      # updated coefficients for the given order
      d0 = np.zeros((nn-1,)*2*iam)
      # double loop over all the combinations of self.a * self.s * self.s * ...
      # such that the total number of indices of the tensor product is equal to
      # the number of indices of d0
      for ii in range(iam,np.sum(self.nquant)+1):
        a = self.a[ii]
        for jj in range(2*(ii-iam),2*ii+1):
          # the part of self.a that is currently needed (assuming no symmetry)
          my_slice = [[] for kk in range(a.ndim)]
          l1 = list(itol.combinations(range(a.ndim),2*ii-jj))
          l2 = list(itol.combinations(range(a.ndim),a.ndim-2*ii+jj))[::-1]
          for kk in range(len(l1)):
            lnow = l1[kk]
            for ll in lnow: my_slice[ll] = slice(0,nn-1)
            lnow = l2[kk]
            for ll in lnow: my_slice[ll] =  nn-1
            if not kk: d1 = a[tuple(my_slice)]
            else:      d1 = d1 + a[tuple(my_slice)]
          # the tensor products self.s * self.s * ... that are needed
          d2 = np.array(1.0)
          #d2 = np.ones((nn-1,)*(2*iam-2*ii+jj))
          for kk in range(2*iam-2*ii+jj):
            if not kk: d2 = np.reshape(self.s[:nn-1,nn-1],(1,)*kk + \
                              (nn-1,)+(1,)*(2*iam-2*ii+jj-kk-1))
            else: d2 = d2 * np.reshape(self.s[:nn-1,nn-1],(1,)*kk + \
                              (nn-1,)+(1,)*(2*iam-2*ii+jj-kk-1))
          # form tensor products of self.a- and self.s-parts
          d3 = np.reshape(d1,np.shape(d1)+(1,)*d2.ndim) * np.reshape(d2,(1,) * \
                d1.ndim+np.shape(d2))
          # add all contributions with the same order (no symmetry)
          d0 = d0 + (-1)**jj * fac(jj) /                                       \
                 (4.**(ii-iam) * fac(ii-iam) * fac(jj-2*(ii-iam)))             \
                    * d3 / self.s[nn-1,nn-1]**(jj+iam-ii)
      d0 = d0 * np.sqrt(np.pi/self.s[nn-1,nn-1])
      a_new.append(d0)
    self.a = copy(a_new)
  
  def permute(self,nuc=None):
    '''
    Permute the order of the nuclei in the transformation matrix tmatrix.
    
    If called without argument, it just rolls the coordinates such that the 
    last nucleus becomes the first, the first the second, etc. After each call,
    the integration should be repeated to get the density for the next nucleus.
    
    If called with parameter nuc, will roll until the given nucleus is the 
    first.
    '''
    if not nuc:
      self.tmatrix = np.roll(self.tmatrix,-self.ndim,axis=1)
    else:
      self.tmatrix = np.roll(self.tmatrix,-self.ndim*nuc,axis=1)
    
  def update_rho(self,nuc):
    '''
    Compute density for nucleus nuc in displacement coordinates on a grid given
    by self.x1, self.x2, self.x3 and store in self.rho[nuc]
    '''
    # compute density in terms of displacement coordinates for some grid
    rho = 0
    for a in self.a:
      for index in itol.product(range(self.ndim),repeat=a.ndim):
        dummy = 1.
        for iam in index:
          dummy *= self.xmesh[iam]
        rho += a[index] * dummy
    for jj in range(self.ndim):
      for kk in range(self.ndim):
        rho *= np.exp(-self.s[jj,kk]*self.xmesh[jj]*self.xmesh[kk])
    print('Norm: ' + str(np.sum(rho)*self.dv))
    # store density separately
    self.rho[nuc] = rho
  
  def get_grid(self,nuc=0):
    '''
    Note that this function only works properly if the mass of the nuclei 
    in self.masses in in amu. It determines the grid by assuming that the 
    extend of the nuclear density shrinks proportional to sqrt(mass).
    '''
    xmax = self.xmax/np.sqrt(self.masses[nuc*self.ndim])
    self.x1 = np.linspace(-xmax, xmax, self.ngrid)
    self.x2 = np.linspace(-xmax, xmax, self.ngrid)
    self.x3 = np.linspace(-xmax, xmax, self.ngrid)
    # save displacement grid for current nucleus in atomic units
    self.x1rel[nuc] = copy(self.x1)
    self.x2rel[nuc] = copy(self.x2)
    self.x3rel[nuc] = copy(self.x3)
    # convert to internal units
    self.x1 *= self.c_length
    self.x2 *= self.c_length
    self.x3 *= self.c_length
    # set grid parameters
    self.dv = (self.x1[-1]-self.x1[0])/float(len(self.x1)-1) * \
              (self.x2[-1]-self.x2[0])/float(len(self.x2)-1) * \
              (self.x3[-1]-self.x3[0])/float(len(self.x3)-1)
    self.xmesh = np.meshgrid(self.x1, self.x2, self.x3, indexing='ij')
    self.nx = np.array([len(self.x1), len(self.x2), len(self.x3)])
  
  def mqnmc(self):
    '''
    Convenience function that calls all necessary functions to determine the
    nuclear density or the nuclear densities for each nucleus separately
    '''
    print('This is the mQNMc v0.1')
    print('')
    self.initialize()
    self.get_normal_modes()
    self.print_information()
    self.set_width_trans_rot()
    self.get_transformation_matrix()
    # loop over all nuclei
    for iam in range(len(self.nuclei)):
      print(' ')
      print('Computing density for '+self.nuclei[iam]+' nucleus '+str(iam+1)+' of '+str(len(self.nuclei))+'.')
      self.init_poly_param()
      self.init_gaus_param()
      self.integrate()
      # compute automatic grid for each nucleus if triggered
      if self.ngrid:
        self.get_grid(nuc=iam)
      self.update_rho(nuc=iam)
      self.permute()
      print("Computed density for nucleus "+str(iam+1)+" of "+str(len(self.nuclei))+'.')
    # print norm to check if it corresponds to the number of nuclei.
    #print("Norm: " + str(np.sum(self.rho)*self.dv))
    self.finalize()
    print('')
    print('KTHXBYE!')
    print('')
  
  def construct_full_density(self,X):
    '''
    Construct the all-nuclei density from the densities of each nucleaus by 
    centering the densities computed in displacement coordinates around the 
    location of the respective nucleus and by interpolating these densities on 
    the input grid X = [X1, X2, X3], where X1, X2, X3 are regular 
    one-dimensional numpy arrays of the grid on which to get the density.
   
    Returns the density on that grid.
    '''
    Xmesh = np.meshgrid(X[0],X[1],X[2],indexing='ij')
    nX = np.array([len(X[0]),len(X[1]),len(X[2])])
    Xlist = [np.reshape(Xmesh[0],np.prod(nX)),\
             np.reshape(Xmesh[1],np.prod(nX)),\
             np.reshape(Xmesh[2],np.prod(nX))]
    rho_full = np.zeros(nX)
    for nuc, rho in enumerate(self.rho):
      try:
        dummy = interpol.RegularGridInterpolator(\
          (self.x1+self.X0[nuc,0], \
           self.x2+self.X0[nuc,1], \
           self.x3+self.X0[nuc,2]), rho, bounds_error=False, fill_value=0.)
        rho_full += np.reshape(dummy(np.transpose(Xlist)),nX)
      except:
        print('RegularGridInterpolator is missing.')
    
    rho_full[rho_full==0]=1e-100
    return rho_full
  
  def plot_classical_modes(self):
    print('*** Function <plot_classical_modes> is wrong... ***')
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    plt.ion()
    
    tmi = np.linalg.inv(self.tmatrix)
    #tmi = self.tmatrix
    #mode = np.zeros(self.nmodes)
    iselect = 2
    #mode[iselect-1] = 1
    displacement = np.reshape(tmi.transpose()[-iselect],np.shape(self.X0))
    #np.reshape(np.dot(tmi,mode),np.shape(self.X0),order='F')
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    col = {'H':'white','C':'black','N':'blue','O':'red','S':'green'}
    size0 = 100
    ns = 10
    size = np.linspace(10,0.9*size0,ns)[::-1]
    for ii, nuc in enumerate(self.nuclei):
      try:
        ax.scatter(self.X0[0,ii],self.X0[1,ii],self.X0[2,ii],s=size0,c=col[nuc])
        for iam, iscale in enumerate(np.linspace(0,0.3,ns)):
          ax.scatter(self.X0[0,ii]+iscale*displacement[0,ii],
                     self.X0[1,ii]+iscale*displacement[1,ii],
                     self.X0[2,ii]+iscale*displacement[2,ii],s=size[iam],c=col[nuc])
          ax.scatter(self.X0[0,ii]-iscale*displacement[0,ii],
                     self.X0[1,ii]-iscale*displacement[1,ii],
                     self.X0[2,ii]-iscale*displacement[2,ii],s=size[iam],c=col[nuc])
      except:
        ax.scatter(self.X0[0,ii],self.X0[1,ii],self.X0[2,ii],s=size0,c='black')
  
  def plot_normal_modes(self):
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    plt.ion()
    
    for iselect in range(self.ntrans+self.nrot,self.nmodes):
      displacement = np.reshape(self.normal_modes[:,iselect],np.shape(self.X0))
      
      fig = plt.figure('mode '+str(iselect+1))
      ax = fig.add_subplot(111, projection='3d')
      col = {'H':'gray','C':'black','N':'blue','O':'red','S':'green',\
             'F':'orange','SI':'yellow'}
      size0 = 100
      ns = 10
      size = np.linspace(10,0.9*size0,ns)[::-1]
      for ii, nuc in enumerate(self.nuclei):
        try:
          for iam, iscale in enumerate(np.linspace(0,1.0,ns)):
            ax.scatter(self.X0[ii,0]+iscale*displacement[ii,0],
                       self.X0[ii,1]+iscale*displacement[ii,1],
                       self.X0[ii,2]+iscale*displacement[ii,2],s=size[iam],c=col[nuc], edgecolor='none')
            ax.scatter(self.X0[ii,0]-iscale*displacement[ii,0],
                       self.X0[ii,1]-iscale*displacement[ii,1],
                       self.X0[ii,2]-iscale*displacement[ii,2],s=size[iam],c=col[nuc], edgecolor='none')
          ax.scatter(self.X0[ii,0],self.X0[ii,1],self.X0[ii,2],s=size0,c=col[nuc])
        except:
          print('Nucleus type '+nuc+' has no color yet.')
          print('Falling back to default color black')
          for iam, iscale in enumerate(np.linspace(0,1.0,ns)):
            ax.scatter(self.X0[ii,0]+iscale*displacement[ii,0],
                       self.X0[ii,1]+iscale*displacement[ii,1],
                       self.X0[ii,2]+iscale*displacement[ii,2],s=size[iam],c=col[nuc], edgecolor='none')
            ax.scatter(self.X0[ii,0]-iscale*displacement[ii,0],
                       self.X0[ii,1]-iscale*displacement[ii,1],
                       self.X0[ii,2]-iscale*displacement[ii,2],s=size[iam],c='black')
          ax.scatter(self.X0[ii,0],self.X0[ii,1],self.X0[ii,2],s=size0,c='black')
