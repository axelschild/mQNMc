import numpy as np
import matplotlib.pyplot as plt

def plot_density(x,y,data,contours,colormaps):
  '''
  Function for making contour plots of the density in the molecular plane.
  '''
  try:
    len(contours[0])
  except:
    contours  = [contours]
    colormaps = [colormaps]
  
  for ii in range(len(contours)):
    plt.contourf(x, y, data, contours[ii], cmap=colormaps[ii])
  
  return
