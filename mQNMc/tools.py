'''
mQNCc
by Axel Schild

This file is part of mQNCc.

mQNCc is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

mQNCc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with mQNCc.  If not, see <http://www.gnu.org/licenses/>.

This file contains some tools.
'''

import numpy as np
import time
import sys
import h5py


def flatten(lot):
  '''
  flatten a list ot tuple by recurrence
  '''
  for item in lot:
    if isinstance(item, (list, tuple)):
      for subitem in flatten(item): yield subitem
    else:
      yield item

def saveToHDF5(fname,stuff,mode='w'):
  '''
  Save all elements of the dictionary stuff to the HDF5 file fname.
  '''
  f = h5py.File(fname, mode)
  for name, ding in stuff.items():
    ding = np.squeeze(ding)
    if len(np.shape(ding)) == 1:
      dset = f.create_dataset(name,(1,len(ding)),data=ding)
    else:
      dset = f.create_dataset(name,(np.shape(ding)),data=ding)
  f.close()


def loadFromHDF5(fname,stuffList=[]):
  '''
  Load all elements of the dictionary stuff or all elements from the HDF5 file.
  
  Use
   for key,val in data.items(): exec(key + '=val')
  to load all of them in the current workspace.
  '''
  f = h5py.File(fname, 'r')
  returnList = {}
  if stuffList:
    for name in stuffList:
      returnList[name] = np.squeeze(f[name][...])
  else:
    for (name,data) in f.items():
      returnList[name] = np.squeeze(data[...])
  f.close()
  return returnList

def saveMolToHDF5(fname,mol):
  print(' Warning: saveMolToHDF5 does not save mol.nuclei (the names of the nuclei)!')
  print(' Warning: saveMolToHDF5 does not save mol.a and mol.s (the parameters of the polynomial)!')
  stuff = {'F_SI'           : mol.F_SI,
           'F_linear'       : mol.F_linear,
           'X0'             : mol.X0,
           #'a'              : mol.a,
           'c_energy'       : mol.c_energy,
           'c_length'       : mol.c_length,
           'c_mass'         : mol.c_mass,
           'c_time'         : mol.c_time,
           'dv'             : mol.dv,
           'eigenvalues'    : mol.eigenvalues,
           'force_constants': mol.force_constants,
           'freq_trans_rot' : mol.freq_trans_rot,
           'frequencies'    : mol.frequencies,
           'hbar'           : mol.hbar,
           'lightspeed'     : mol.lightspeed,
           'masses'         : mol.masses,
           'ndim'           : mol.ndim,
           'ngrid'          : mol.ngrid,
           'nmodes'         : mol.nmodes,
           'normal_modes'   : mol.normal_modes,
           'normal_modes_nmw': mol.normal_modes_nmw,
           'nquant'          : mol.nquant,
           'nrot'            : mol.nrot,
           'ntrans'          : mol.ntrans,
           'nvib'            : mol.nvib,
           'nx'              : mol.nx,
           'rho'             : mol.rho,
           #'s'               : mol.s,
           'tmatrix'         : mol.tmatrix,
           'x1'              : mol.x1,
           'x1rel'           : mol.x1rel,
           'x2'              : mol.x2,
           'x2rel'           : mol.x2rel,
           'x3'              : mol.x3,
           'x3rel'           : mol.x3rel,
           'xmax'            : mol.xmax
           }
  saveToHDF5(fname,stuff,mode='w')

def loadMolFromHDF5(fname):
  from molecule import molecule
  data = loadFromHDF5(fname)
  mol  = molecule()
  mol.F_SI             = data['F_SI']
  mol.F_linear         = data['F_linear']
  mol.X0               = data['X0']
  #mol.a                = data['a']
  mol.c_energy         = data['c_energy']
  mol.c_length         = data['c_length']
  mol.c_mass           = data['c_mass']
  mol.c_time           = data['c_time']
  mol.dv               = data['dv']
  mol.eigenvalues      = data['eigenvalues']
  mol.force_constants  = data['force_constants']
  mol.freq_trans_rot   = data['freq_trans_rot']
  mol.frequencies      = data['frequencies']
  mol.hbar             = data['hbar']
  mol.lightspeed       = data['lightspeed']
  mol.masses           = data['masses']
  mol.ndim             = data['ndim']
  mol.ngrid            = data['ngrid']
  mol.nmodes           = data['nmodes']
  mol.normal_modes     = data['normal_modes']
  mol.normal_modes_nmw = data['normal_modes_nmw']
  mol.nquant           = data['nquant']
  mol.nrot             = data['nrot']
  mol.ntrans           = data['ntrans']
  mol.nvib             = data['nvib']
  mol.nx               = data['nx']
  mol.rho              = data['rho']
  #mol.s                = data['s']
  mol.tmatrix          = data['tmatrix']
  mol.x1               = data['x1']
  mol.x1rel            = data['x1rel']
  mol.x2               = data['x2']
  mol.x2rel            = data['x2rel']
  mol.x3               = data['x3']
  mol.x3rel            = data['x3rel']
  mol.xmax             = data['xmax ']
  print(' Warning: mol.nuclei is not loaded.')
  print(' Warning: mol.a and mol.s are not loaded.')
  return mol

def density_autogrid(X0,dX=.1,flat=False,dims=3,xadd=1.):
  # test for which direction the molecule is planar
  iflat = 0
  if flat:
    for ii in range(dims):
      if not np.sum(np.abs(X0[:,ii])):
        iflat = ii+1
        print('Select direction '+str(iflat)+' of '+str(dims)+' to be plane.')
    if not iflat:
      print('Molecule seems not to be planar or is not properly oriented.')
      print('Falling back to non-planar calculation...')
      flat = False
  
  # determine minimum and maximum values per direction
  xmin = [[] for ii in range(dims)]
  xmax = [[] for ii in range(dims)]
  for ii in range(dims):
    if X0[:,ii].min(): 
      xmin[ii] = X0[:,ii].min() + np.sign(X0[:,ii].min()) * xadd
    else:
      if flat and iflat == ii+1:
        xmin[ii] = -dX
      else:
        xmin[ii] = X0[:,ii].min() - xadd
    if X0[:,ii].max():
      xmax[ii] = X0[:,ii].max() + np.sign(X0[:,ii].max()) * xadd
    else:
      if flat and iflat == ii+1:
        xmax[ii] = dX
      else:
        xmax[ii] = X0[:,ii].max() + xadd
  # determine number of points per direction
  n = [[] for ii in range(dims)]
  for ii in range(dims):
    if iflat == ii+1:
      n[ii] = 3
    else:
      n[ii] = int(np.ceil((xmax[ii]-xmin[ii])/dX)) + 1
    if not np.mod(n[ii],2): n[ii] += 1
  # correct to get new minimum and maximum values that give exactly dX
  for ii in range(dims):
    if not iflat == ii+1:
      xavg = (xmax[ii]+xmin[ii])/2.
      xmin[ii] = xavg - dX*(n[ii]//2)
      xmax[ii] = xavg + dX*(n[ii]//2)
  
  # determine grid
  X = [[] for ii in range(dims)]
  for ii in range(dims):
    X[ii] = np.linspace(xmin[ii],xmax[ii],n[ii])
  
  return X, dX**dims, iflat-1

