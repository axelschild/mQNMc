'''
mQNCc
by Axel Schild

This file is part of mQNCc.

mQNCc is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

mQNCc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with mQNCc.  If not, see <http://www.gnu.org/licenses/>.

 Some conversion factors to transform to other units
 
 MD units:
  - length in Angstroem
  - mass in atomic mass units
  - time in femtoseconds
  -> energy in amu AA^2 / T^2
'''
import numpy as np

## some constants
#kb_SI = 1.3806485279e-23           # Boltzmann constant in J/K
Na_imol = 6.02214085774e23         # Avogadro's number in 1/mol
c_SI = 299792458                   # speed of light in m/s

# conversion from atomic units to SI units
mass_au__SI   = 9.1093829140e-31   # m_e -> kg
action_au__SI = 1.05457172647e-34  # hbar -> Js
length_au__SI = 5.291772109217e-11 # a_0 -> m
energy_au__SI = 4.3597441775e-18   # E_h -> J
time_au__SI   = 2.418884326505e-17 # hbar/E_h -> m/s

# more conversion factors
mass_u__SI = 1.66053904020e-27     # atomic mass unit -> kg
mass_u__au = 1822.88839            # atomic mass unit -> m_e
energy_eV__SI = 1.60217656535e-19  # eV -> J
energy_cal__SI = 4.184             # cal -> J

time_SI__fs = 1e15
time_fs__SI = 1./time_SI__fs
length_SI__AA = 1e10
length_AA__SI = 1./length_SI__AA

mass_au__u = 1./mass_u__au
time_au__fs = time_au__SI * time_SI__fs
length_au__AA = length_au__SI * length_SI__AA
length_AA__au = 1./length_au__AA

hbar_SI = action_au__SI            # hbar in J

energy_md__SI = mass_u__SI * length_AA__SI**2 / time_fs__SI**2
energy_SI__icm = 1/(c_SI*hbar_SI*2*np.pi)/100.               # J -> 1/cm
energy_md__icm = energy_md__SI * energy_SI__icm

energy_kcal_imol__SI = 1000*energy_cal__SI/Na_imol          # kcal/mol -> J
energy_SI__kcal_imol = 1./energy_kcal_imol__SI
energy_au__kcal_imol = energy_au__SI * energy_SI__kcal_imol
energy_au__icm = energy_au__SI * energy_SI__icm

c_au = c_SI / length_au__SI * time_au__SI
