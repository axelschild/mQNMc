'''
mQNCc
by Axel Schild

This file is part of mQNCc.

mQNCc is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

mQNCc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with mQNCc.  If not, see <http://www.gnu.org/licenses/>.

This file contains the reader for a Psi4 file.
'''

import numpy as np
#np.set_printoptions(linewidth=999999)

def read_psi4(filename):
  '''
  Needs to read
   -- name of nuclei
   -- masses of nuclei
   -- equilibrium position of nuclei
   -- hessian
  '''
  
  mass_con = 1./5.48579909016e-4
  
  fid    = open(filename,'r')      # Open the file
  flines = fid.readlines()         # Read the WHOLE file into RAM
  fid.close()                      # Close the filename
  
  nuc_names = []
  nuc_masses = []
  nuc_positions = []
  nuc_hessian = []
  
  for il, line in enumerate(flines):
    #thisline = line.split()         # The current line split into segments
    if 'Geometry (in' in line:
      nuc_names = []
      nuc_positions = []
      if 'Mass' in flines[il+2]: nuc_masses = []
      if 'Angstrom' in line: length_con = 1./.5291772109217
      else: length_con = 1.
      ii = il+4
      while flines[ii].strip():
        nuc_names.append(flines[ii].split()[0])
        nuc_positions.append(flines[ii].split()[1:4])
        if 'Mass' in flines[il+2]: 
          for jj in range(3): nuc_masses.append(flines[ii].split()[4])
        ii += 1
    # PSI4 distributes the Hessian over multiple lines ... not nice, but common
    if '## Hessian' in line:
      # size of Hessian
      n1, n2 = int(flines[il+1].split()[3]), int(flines[il+1].split()[5])
      nuc_hessian = np.zeros([n1,n2])
      # determine in how many parts PSI4 has split the Hessian
      entries_per_part = len(flines[il+3].split())
      number_of_parts  = int( np.ceil( n2 / entries_per_part ) ) 
      
      for i_part in range(number_of_parts):
        istart = il + 5 + i_part*(n1+3)
        for i1 in range(n1):
          i2a = i_part*entries_per_part
          i2b = i_part*entries_per_part + len(flines[istart-2].split())
          nuc_hessian[i1,i2a:i2b] = flines[istart+i1].split()[1:]
      
  return nuc_names, \
         np.array(nuc_masses   ).astype(np.float)*mass_con  , \
         np.array(nuc_positions).astype(np.float)*length_con, \
         nuc_hessian
