'''
mQNCc
by Axel Schild

This file is part of mQNCc.

mQNCc is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

mQNCc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with mQNCc.  If not, see <http://www.gnu.org/licenses/>.

Description:
  init file to make mQNCc a module
'''
# -*- coding: iso-8859-1 -*-
__all__ = ['conversion_factors',
           'molecule',
           'reader_psi4',
           'tools',
           'tools_extra'
           ]

__version__ = '1.0'
