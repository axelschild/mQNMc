This is mQNMc, a python module to compute the one-nucleus density of 
vibrationally excited states within the local harmonic approximation of the 
potential energy surface. The one-nucleus density is the marginal probability 
density of finding any nucleus somewhere in space independent of the location 
of the other nuclei. It provides a direct quantum-mechanical picture of 
(excited states of) the nuclei and illustrates how their relative 
three-dimensional configuration arises from the 3N-dimensional probability 
density.

In the harmonic approximation, the nuclear wavefunction is represented as a 
product of harmonic oscillator wavefunctions along the normal mode coordinates.
mQNMc uses the hessian of a quantum chemistry calculation of a molecule to 
obtain the normal modes, sets up the harmonic oscillator wavefunctions in 
these normal modes for selected excitations, and integrates the nuclear 
probability density over the coordinates of all but one nucleus.

So far, only a reader for PSI4 (www.psicode.org) output files is provided. To 
get started with mQNMc, have a look at the examples and at the documentation of 
the class molecule in molecule.py.

For installation, all you need to do is to add mQNMc to your Python path, e.g. 

export PYTHONPATH=$PYTHONPATH:$HOME/my_python_modules/mQNMc

You might want to add this line to your .bashrc or .zshrc (or equivalent).
