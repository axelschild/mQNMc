'''
mQNCc
by Axel Schild

This file is part of mQNCc.

mQNCc is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

mQNCc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with mQNCc.  If not, see <http://www.gnu.org/licenses/>.

  Plot the normal modes of the OHH molecule.
'''

import numpy as np
# import mQNMc
from   mQNMc.molecule    import molecule
from   mQNMc.reader_psi4 import read_psi4
from   mQNMc.tools       import saveToHDF5
import mQNMc.conversion_factors as conv
# import plotting tools
import matplotlib.pyplot as plt

np.set_printoptions(linewidth=200)

# data file to be loaded
filename = 'C_H2O_water/mp3.psi4.out'
print('Using '+filename+' as input.')

# load data
mol = molecule()
mol.nuclei, mol.masses, mol.X0, mol.force_constants = read_psi4(filename)

# deuterate if necessary by changing the masses and the labels
ideuter = []
for iam in ideuter:
  mol.nuclei[iam] = 'D'
  mol.masses[iam*3:(iam+1)*3] = 3671.4828900941584

# set options and calculate normal modes manually
mol.F_linear = False                # Is molecule linear?
mol.F_SI     = False                # Shall SI-based units be used? (not recommended)
mol.initialize()                    # initialization
mol.get_normal_modes()              # compute normal modes
mol.print_information()             # print information about normal modes
mol.freq_trans_rot = 0.5            # frequency of gaussian for normal modes of translation & rotation
mol.set_width_trans_rot()           # set the frequency for normal modes of translation & rotation
mol.get_transformation_matrix()     # transformation matrix for calculation
mol.finalize()                      # transform back from internal units to input units

# plot all vibrational normal modes that are completely in the YZ-plane
for iselect in range(mol.ntrans+mol.nrot,mol.nmodes):
  normal_modes = mol.normal_modes/np.sqrt(np.reshape(mol.masses,(mol.nmodes,1))/conv.mass_u__au)
  displacement = 0.5*np.reshape(normal_modes[:,iselect],np.shape(mol.X0))
  nmode = iselect+1
  
  # test if its flat w.r.t. YZ-plane & plot
  if (not np.sum(np.abs(displacement[:,0]))) and (not np.sum(np.abs(mol.X0[:,0]))):
    print('Plotting normal mode '+str(nmode)+' in yz-plane...')
    fig = plt.figure()
    ax1 = fig.add_axes([0.1,0.25,0.80,0.70])
    for jj, nuc in enumerate(mol.nuclei):
      ax1.arrow(mol.X0[jj,1],mol.X0[jj,2], displacement[jj,1], displacement[jj,2], linewidth=3, head_width=0.15, head_length=0.15, fc='red', ec='red')
      ax1.arrow(mol.X0[jj,1],mol.X0[jj,2],-displacement[jj,1],-displacement[jj,2], linewidth=3, head_width=0.15, head_length=0.15, fc='orange', ec='orange')
    
    for jj, nuc in enumerate(mol.nuclei):
      ax1.text(mol.X0[jj][1], mol.X0[jj][2], nuc, fontname='STIXGeneral', size=36, va='center', ha='center', clip_on=True)
    
    plt.axis([-2,2,-.5,1.6])
    ax1.set_aspect('equal', 'box')
    ax1.set_xlabel('$R_2 / a_0$',fontsize = 20)
    ax1.set_ylabel('$R_3 / a_0$',fontsize = 20)
    
    plt.xticks([-1.5,0.0,1.5],fontsize = 18)
    plt.yticks([     0.0,1.5],fontsize = 18)
    plt.savefig(''.join(mol.nuclei)+'_mode_'+str(nmode)+'_yz.png',bbox_inches='tight')
    plt.savefig(''.join(mol.nuclei)+'_mode_'+str(nmode)+'_yz.pdf',bbox_inches='tight')
    plt.close()
