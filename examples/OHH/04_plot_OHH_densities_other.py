'''
mQNCc
by Axel Schild

This file is part of mQNCc.

mQNCc is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

mQNCc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with mQNCc.  If not, see <http://www.gnu.org/licenses/>.

  Compute the one-nucleus density of the states corresponding to some selected
  excited states and make contour plots of the density in the molecular plane.
  
  NOTE: The naming of the states is according to energy and hence different 
        from the spectroscopic notation used in the paper!
        
  NOTE: This calculation takes some time because there are higher excited states
        in <nquant_list> for which the calculation takes longer. You might want
        to remove some of these states. Also, if 
          F_save = True
        the calculations will be saved. If 
          F_load = True
        the saved calculations are loaded and no actual computation is done, 
        only the plotting.
        
'''

import numpy as np
np.set_printoptions(linewidth=200)
# import mQNMc
from   mQNMc.molecule      import molecule
from   mQNMc.reader_psi4   import read_psi4
from   mQNMc.tools         import saveMolToHDF5, loadMolFromHDF5
from   mQNMc.tools_special import plot_density
import mQNMc.conversion_factors as conv
# import plotting tools
import matplotlib.pyplot as plt
from   matplotlib.patches import Rectangle

F_save = True   # flag to save output
F_load = False  # flag to load output (is saved previously)

# contours and colormaps used for plotting
contours = [np.linspace(    1,    10,20),
            np.linspace(   10,   100,20),
            np.linspace(  100,  1000,20),
            np.linspace( 1000, 10000,20),
            np.linspace(10000, 30000,20),
            np.linspace(30000, 60000,20)]
colormaps = ['Greys', 'BuPu', 'YlGnBu', 'YlGn', 'YlOrBr','YlOrRd']

# data file to be loaded
filename = 'C_H2O_water/mp3.psi4.out'
print('Using '+filename+' as input.')

# load data
mol = molecule()
mol.nuclei, mol.masses, mol.X0, mol.force_constants = read_psi4(filename)

# deuterate if necessary by changing the masses and the labels
ideuter = []
for iam in ideuter:
  mol.nuclei[iam] = 'D'
  mol.masses[iam*3:(iam+1)*3] = 3671.4828900941584

# generate quantum numbers for excited states
nquant_list = [\
               [0]*6+[2,0,0],\
               [0]*6+[0,2,0],\
               [0]*6+[0,0,2],\
               [0]*6+[1,1,0],\
               [0]*6+[1,0,1],\
               [0]*6+[0,1,1],\
               [0]*6+[3,0,0],\
               [0]*6+[0,3,0],\
               [0]*6+[0,0,3],\
               [0]*6+[2,1,0],\
               [0]*6+[2,0,1],\
               [0]*6+[1,2,0],\
               [0]*6+[0,2,1],\
               [0]*6+[1,0,2],\
               [0]*6+[0,1,2],\
               [0]*6+[1,1,1],\
               [0]*6+[0,3,1]]

for imode, nquant in enumerate(nquant_list):
  # do the actual calculation
  if not F_load:
    mol.nquant = nquant
    mol.freq_trans_rot = 0.5
    mol.mqnmc()
  # load previously calculated data
  else:
    mol = loadMolFromHDF5(fname)
  # save data
  if F_save:
    fname = ''.join(mol.nuclei)+'_data_'+''.join([str(num) for num in nquant[6:]])+'.h5'
    saveMolToHDF5(fname,mol)
  # plotting
  plt.clf()
  for iam in range(len(mol.nuclei)):
    plot_density(mol.X0[iam][1]+mol.x2rel[iam],
                 mol.X0[iam][2]+mol.x3rel[iam],
                 mol.rho[iam][mol.ngrid//2].transpose(),
                 contours,colormaps)
    plt.xlim(-2.0, 2.0)
    plt.ylim(-0.5, 1.6)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel('$R_2 / a_0$' ,fontsize = 20)
    plt.ylabel('$R_3 / a_0$' ,fontsize = 20)
    plt.xticks([-1.5,0.0,1.5],fontsize = 18)
    plt.yticks([     0.0,1.5],fontsize = 18)
  # add magnification of oxygen nuclear density
  main_axis = plt.gca()
  subplot = plt.axes([.5, .24, .3, .3])
  plot_density(mol.X0[0][1]+mol.x2rel[0],
               mol.X0[0][2]+mol.x3rel[0],
               mol.rho[0][mol.ngrid//2].transpose(),
               contours,colormaps)
  plt.xlim(-.07, .07)
  plt.ylim(-0.22, -0.03)
  plt.xticks([])
  plt.yticks([])
  sub_axis = plt.gca()
  sub_axis.set_aspect('equal', adjustable='box')
  xmin, xmax = sub_axis.get_xlim()
  ymin, ymax = sub_axis.get_ylim()
  main_axis.add_patch(Rectangle((xmin, ymin), xmax-xmin, ymax-ymin, alpha=1, facecolor='none',edgecolor='silver'))
  plt.savefig(''.join(mol.nuclei)+'_density_'+''.join([str(num) for num in nquant[6:]])+'_yz.png',bbox_inches='tight')
  plt.savefig(''.join(mol.nuclei)+'_density_'+''.join([str(num) for num in nquant[6:]])+'_yz.pdf',bbox_inches='tight')
  plt.close()
